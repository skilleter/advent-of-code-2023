#!/usr/bin/env python3

"""Advent of Code 2023"""

import sys

################################################################################

def error(msg):
    """Report an error and quit"""

    print(msg)
    sys.exit(1)

################################################################################

def next_value(sequence):
    triangle = [sequence]

    print('-'*80)
    print(f'Sequence={sequence}')
    print()

    while True:
        new_line = []
        for i in range(len(triangle[-1])-1):
            new_line.append(triangle[-1][i+1] - triangle[-1][i])

        triangle.append(new_line)
        if new_line == [0]*len(new_line):
            break

    print('Triangle:')
    for line in triangle:
        print(line)

    print()

    triangle[-1].append(0)
    for i in range(len(triangle)-2, -1, -1):
        new_value = triangle[i][-1] + triangle[i+1][-1]
        triangle[i].append(new_value)

    print('Updated triangle:')
    for line in triangle:
        print(line)

    # Sanity check

    for i in range(len(triangle)-1):
        for j in range(len(triangle[i])-1):
            if triangle[i][j+1] - triangle[i][j] != triangle[i+1][j]:
                error(f'Invalid triangle at row {i}, column {j}')

    if sum(triangle[-1]) != 0:
        error(f'Invalid final row in triangle')

    result = triangle[0][-1]

    print()
    print(f'Result={result}')
    return result

################################################################################

def main():
    datafile = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    total = 0

    with open(datafile, 'rt') as infile:
        for data in infile.readlines():
            sequence = [int(x) for x in data.strip().split(' ')]

            total += next_value(sequence)

    print('-'*80)
    print(f'Result = {total}')

################################################################################

main()
