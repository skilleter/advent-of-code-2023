#!/usr/bin/env python3

"""Advent of Code 2023"""

# Day 18 gold using shoelace algorithm instead of flood-fill as flood-fill would
# take aeons (and then some!).
import sys

################################################################################

DEBUG = False

################################################################################

def error(msg):
    """Report an error and quit"""

    print(msg)
    sys.exit(1)

################################################################################

def debug(msg):
    """Conditional print"""

    if DEBUG:
        print(msg)

################################################################################

def shoelace(vertices):
    """A function to apply the Shoelace algorithm"""
    
    numberOfVertices = len(vertices)
    sum1 = 0
    sum2 = 0
  
    for i in range(0,numberOfVertices-1):
        sum1 = sum1 + vertices[i][0] *  vertices[i+1][1]
        sum2 = sum2 + vertices[i][1] *  vertices[i+1][0]
  
    #Add xn.y1
    
    sum1 = sum1 + vertices[numberOfVertices-1][0]*vertices[0][1]   
    
    #Add x1.yn
    
    sum2 = sum2 + vertices[0][0]*vertices[numberOfVertices-1][1]   
    
    area = abs(sum1 - sum2) / 2
    return area

################################################################################

def main():
    """Entry point"""

    datafile = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    x = y = 0

    trench = [[0,0]]
    trench_len = 0

    with open(datafile, 'rt') as infile:
        for data in infile.readlines():
            commands = data.strip().split(' ')
            colour = commands[2][2:-1]

            distance = int(colour[:-1], 16)
            direction = colour[-1]

            trench_len += distance

            debug(f'{direction} : {distance} : {colour}')

            if direction == '3':
                y -= distance

            elif direction == '1':
                y += distance

            elif direction == '2':
                x -= distance

            elif direction == '0':
                x += distance

            else:
                error(f'Invalid direction: {direction}')

            trench.append([x,y])

    area = shoelace(trench)

    print(f'Trench length = {trench_len}')
    print(f'Area = {area + trench_len/2 + 1}')

################################################################################

main()
