#!/usr/bin/env pypy3

"""Advent of Code 2023"""

import sys

################################################################################

DEBUG = True
VERBOSE = False

################################################################################

def error(msg):
    """Report an error and quit"""

    print(msg)
    sys.exit(1)

################################################################################

def debug(msg):
    """Conditional print"""

    if DEBUG:
        print(msg)

################################################################################

def verbose(msg):
    """Conditional waffle"""

    if DEBUG and VERBOSE:
        print(msg)

################################################################################

def read_data():
    """Read the data"""

    bricks = []

    datafile = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    with open(datafile, 'rt') as infile:
        for data in infile.readlines():
            brickdata = data.strip().split('~')
            x0, y0, z0 = [int(n) for n in brickdata[0].split(',')]
            x1, y1, z1 = [int(n) for n in brickdata[1].split(',')]

            # Get each pair of coordinates in ascending order

            x0, x1 = (x0, x1) if x0 < x1 else (x1, x0)
            y0, y1 = (y0, y1) if y0 < y1 else (y1, y0)
            z0, z1 = (z0, z1) if z0 < z1 else (z0, z1)

            # Each brick is a list of x,y,z coodinates comprising the component cubes within it

            brick = []

            for x in range(x0, x1+1):
                for y in range(y0, y1+1):
                    for z in range(z0, z1+1):
                        brick.append([x,y,z])

            bricks.append(brick)

    return bricks

################################################################################

def move_down(bricks, n):
    """Check if the new location of brick #n would overlap with other
       bricks, if we moved it down 1 block.
       Returns True if it can move. """

    # Create a set of cubes corresponding to the location of the
    # brick under consideration if it moved down on the Z axis

    new_brick = []
    for cube in bricks[n]:
        new_brick.append([cube[0], cube[1], cube[2]-1])

    # Iterate through the bricks, skipping the one that's trying to move

    for i, brick in enumerate(bricks):
        if i != n:
            # Iterate through the cubes in the new brick location
            # if any of them are in the current brick we're looking
            # at then the movement can't take place

            for cube in brick:
                if cube in new_brick:
                    return False

    return True

#########################################################################

def drop(bricks):
    """Try and move the bricks vertically if there's nothing in the way
       Return True if at least one brick moved"""

    dropped = False

    # Iterate through the bricks

    for i, brick in enumerate(bricks):
        dropping = True

        while dropping:
            # If lowest Z of brick is ground level then it can't move any further

            if brick[0][2] == 1:
                verbose(f'Brick #{i} has hit the ground')
                dropping = False

            elif not move_down(bricks, i):
                # New position would overlap another brick, so it can't move

                verbose(f'Brick #{i} is supported by another brick')
                dropping = False

            else:
                # Nothing in the way, so update the brick Z coordinates

                for j in range(len(brick)):
                    brick[j][2] -= 1

                verbose(f'Brick #{i} has dropped')
                dropped = True

    verbose('Bricks have dropped' if dropped else 'No bricks have dropped')

    return dropped

################################################################################

def support_bricks(bricks, brick, i):
    """Return the set of bricks directly under the specified brick with index i"""

    supports = set()

    for n, under in enumerate(bricks):
        if n != i:
            for cube in brick:
                if [cube[0], cube[1], cube[2]-1] in under:
                    supports.add(n)
                    break

    return supports

################################################################################

def what_would_drop(bricks, supporting, gone):
    """Count the number of bricks that would fall if the specified brick was gone"""

    removed = {gone}
    dropping = True

    while dropping:
        dropping = False

        # Check each of the remaining bricks

        for brk in range(len(bricks)):
            # If the brick hasn't alrady been removed

            if brk not in removed:
                # And isn't on the ground

                if bricks[brk][0][2] > 1:
                    # And if we remove all the bricks tagged for removal then it has no remaining supports

                    if supporting[brk] - removed == set():
                        # Then we continue to drop bricks and we add the current brick to the list that's removed

                        dropping = True
                        removed.add(brk)

    # We want the count of bricks that would drop, so ignore the one that's gone

    removed.remove(gone)

    print(f'If brick #{gone} is removed then bricks {removed} would all fall')

    return len(removed)

################################################################################

def main():
    """Main"""

    # Read the brick layout as a set of bricks, each of which is a set of cube
    # coordinates in ascending coordinate order

    bricks = read_data()

    print('-'*80)
    print('Initial brick configuration')
    for brick in bricks:
        print(brick)

    # Keep dropping the bricks down the Z axis until none of them can move

    print('Dropping the bricks until none of them can move any further')

    while drop(bricks):
        pass

    print('-'*80)
    print('Brick configuration after dropping all of them')
    for brick in bricks:
        print(brick)

    # Now see which bricks have bricks supporting them

    supporting = []
    for i, brick in enumerate(bricks):
        supporting.append(support_bricks(bricks, brick, i))

    print('-'*80)
    print('Brick configuration after dropping all of them')
    for i, supports in enumerate(supporting):
        print(f'Brick #{i} is supported by {supports}')

    # For each brick - count the bricks of which it is the sole support
    # and would drop if it was removed, then count the bricks that would
    # drop when the dropped bricks moved, and so on.

    print('-'*80)

    total = 0
    for gone in range(len(bricks)):
        count = what_would_drop(bricks, supporting, gone)
        total += count

    print('-'*80)
    print(f'Total droppage count = {total}')

################################################################################

main()
