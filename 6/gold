#!/usr/bin/env pypy3

"""Advent of Code 2023"""

import sys
import re
import math

################################################################################

def run_race(race_duration, record_distance):
    print('-'*80)
    print(f'Running race with duration {race_duration} with record distance of {record_distance}')

    wins = 0

    for charge in range(1, race_duration):
        distance = (race_duration-charge)*charge

        if charge % 1000 == 0:
            print(f'    Charge time {charge}. Distance travelled {distance}')

        if distance > record_distance:
            wins += 1

    return wins

################################################################################

def main():
    datafile = sys.argv[1] if len(sys.argv) > 1 else 'gold-data.txt'

    with open(datafile, 'rt') as infile:
        for data in infile.readlines():
            data = re.sub(' +', ' ', data.strip()).split(':')
            print(data)
            values = [int(x) for x in data[1].strip().split(' ')]
            if data[0] == 'Time':
                times = values
            elif data[0] == 'Distance':
                distances = values
            else:
                assert data[0]

    assert len(times) == len(distances)

    wins = []
    for race in zip(times, distances):
        wins.append(run_race(race[0], race[1]))

    result = math.prod(wins)

    print(f'Wins: {wins}')
    print(f'Result: {result}')

################################################################################

main()
