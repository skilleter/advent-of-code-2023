#!/usr/bin/env python3

"""Advent of Code 2023"""

import sys

################################################################################

DEBUG = False

################################################################################

def error(msg):
    """Report an error and quit"""

    print(msg)
    sys.exit(1)

################################################################################

def debug(msg):
    """Conditional print"""

    if DEBUG:
        print(msg)

################################################################################

def main():
    """Entry point"""

    datafile = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    h_splitter = []
    v_splitter = []
    left_mirror = []
    right_mirror = []

    width = 0

    with open(datafile, 'rt') as infile:
        y = 0
        for data in infile.readlines():
            gridline = list(data.strip())
            if not width:
                width = len(gridline)

            for x in range(width):
                item = gridline[x]

                if item == '/':
                    right_mirror.append([x,y])
                elif item == '\\':
                    left_mirror.append([x,y])
                elif item == '|':
                    v_splitter.append([x,y])
                elif item == '-':
                    h_splitter.append([x,y])
            y += 1

    height = y

    # Start with one beam, entering from top left, travelling right

    light = [{'x': -1, 'y': 0, 'xi': 1, 'yi':0}]

    # Set of energised spaces

    energised = set()

    # History of position and direction of light beams

    history = []

    print('Initial conditions:')
    print(f'  Light:         {light}')
    print(f'  Right mirrors: {right_mirror}')
    print(f'  Left mirrors:  {left_mirror}')
    print(f'  H splitters:   {h_splitter}')
    print(f'  V splitters:   {v_splitter}')
    print(f'  Energised:     {energised}')

    # Iterate whilst we have active light beams

    breakpoint()
    while light:
        if DEBUG:
            debug('-'*80)
            debug(f'  Light:         {light}')
            debug(f'  Energised:     {energised}')
        else:
            print(f'Light: {len(light)}, energised: {len(energised)}')

        # Move the first light beam.

        light[0]['x'] += light[0]['xi']
        light[0]['y'] += light[0]['yi']

        if light[0]['x'] < 0 or light[0]['x'] >= width or light[0]['y'] < 0 or light[0]['y'] >= height:
            # Beam has exited the grid, so discard it

            debug(f'{light[0]} has exited - discarding')

            light = light[1:]

        elif light[0] in history:
            # Already had light here going the same way, so no need to carry on

            debug(f'{light[0]} already in history - discarding')

            light = light[1:]

        else:
            # Update light history and energised set

            history.append(light[0].copy())

            energised.add((light[0]['x'], light[0]['y']))

            # See if light has hit a mirror or a splitter

            if [light[0]['x'], light[0]['y']] in right_mirror:
                # Right mirror: '/' - light turns a right angle

                debug(f'{light} hit "/" mirror, turning')

                if light[0]['xi'] == 1:
                    light[0]['xi'] = 0
                    light[0]['yi'] = -1

                elif light[0]['xi'] == -1:
                    light[0]['xi'] = 0
                    light[0]['yi'] = 1

                elif light[0]['yi'] == 1:
                    light[0]['xi'] = -1
                    light[0]['yi'] = 0

                else:
                    light[0]['xi'] = 1
                    light[0]['yi'] = 0

            elif [light[0]['x'], light[0]['y']] in left_mirror:
                # Left mirror: '\' - light turns a right angle

                debug(f'{light} hit "\\" mirror, turning')

                if light[0]['xi'] == 1:
                    light[0]['xi'] = 0
                    light[0]['yi'] = 1

                elif light[0]['xi'] == -1:
                    light[0]['xi'] = 0
                    light[0]['yi'] = -1

                elif light[0]['yi'] == 1:
                    light[0]['xi'] = 1
                    light[0]['yi'] = 0

                else:
                    light[0]['xi'] = -1
                    light[0]['yi'] = 0

            elif [light[0]['x'], light[0]['y']] in h_splitter:
                # Horizontal splitter: '-' - if moving vertically, split into two horizontal beams

                if light[0]['xi'] == 0:
                    debug(f'{light} hit horizontal splitter "-" - splitting')

                    light[0]['xi'] = -1
                    light[0]['yi'] = 0

                    new_light = {'x': light[0]['x'], 'y': light[0]['y'], 'xi': 1, 'yi': 0}
                    light.append(new_light)

            elif [light[0]['x'], light[0]['y']] in v_splitter:
                # Vertical splitter: '|' - if moving horizontally, split into two vertical beams

                if light[0]['yi'] == 0:
                    debug(f'{light} hit vertical splitter "|" - splitting')

                    light[0]['xi'] = 0
                    light[0]['yi'] = -1

                    new_light = {'x': light[0]['x'], 'y': light[0]['y'], 'xi': 0, 'yi': 1}
                    light.append(new_light)

    debug('-'*80)
    debug(f'Energised tiles: {energised}')

    print('-'*80)
    print(f'Total number of energised tiles: {len(energised)}')

################################################################################

main()
